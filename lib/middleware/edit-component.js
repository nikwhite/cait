var _ = require('lodash')
var registry = require('../modules/registry')
var logger = new (require('../util/logger'))('EditComponent')

module.exports = function editComponent(req, res, next) {
	var _rev = req.body._rev
	var data = _.omit(req.body, '_rev')
	
	registry.createAndSave(req.params.component, req.params.id, data, _rev)
	        .then( function(){	
	        	res.sendStatus(200)

	        }, function (status) {
	        	res.sendStatus(status)
	        } )
}