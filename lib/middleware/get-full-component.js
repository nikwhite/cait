var _ = require('lodash')
var db = require('../db/db').db
var registry = require('../modules/registry')

module.exports = function getFullComponent (req, res, next) {
	db.get(req.componentPath, function (err, result) {

		if (err) {
			return res.sendStatus(err.headers.status)
		}

		res.json( _.extend( result, { model: registry.getComponent(req.params.component).model } ) )

	})
}