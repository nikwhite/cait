var _ = require('lodash')
var logger = new (require('../util/logger'))('ListComponents');
var fsUtil = require('../util/fs')
var config = require('../../config/config')
var path = process.cwd() + config.componentDir
/**
 * Find all the user-created page templates in directory 
 * provided by the user in config file (for now static dir)
 * @return {[type]} [description]
 */
function listPageTemplates(req, res, next) {
	
	logger.log('Request for ' + path)

	fsUtil.directoryList(path).then( 

		function success (list) {
			logger.log('Result for ' + path)
			logger.log(list)

			res.json( {
				components: _.map(list, function (filename) { 
					return fsUtil.trimExtension(filename) 
				})
			} )
		},
		
		function failure (err) {
			logger.log('Failed listing ' + path)
			logger.log(err)
			res.sendStatus(404)
		}
	)
}

module.exports = listPageTemplates