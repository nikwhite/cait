var _ = require('lodash')
var logger = new (require('../util/logger'))('CreateComponent')
var registry = require('../modules/registry')

module.exports = function createComponent (req, res, next) {

	registry.createAndSave(req.params.component, req.params.id, req.body)
	        .then( function(){	
	        	res.sendStatus(200)

	        }, function (status) {
	        	res.sendStatus(status)
	        } )
}