var db = require('../db/db')
var logger = new (require('../util/logger'))()

module.exports = function componentExists(req, res, next) {
	
	db.docExists( req.componentPath ).then( function exists () {
		//etag is returned (arguments[0]) and could be added to the header of the response if needed  	
	  	res.sendStatus(200)

	}, function doesNotExist () {
		res.sendStatus(404)
	})
}