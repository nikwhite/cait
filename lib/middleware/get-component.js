var _ = require('lodash')
var db = require('../db/db').db

module.exports = function getComponent (req, res, next) {

	db.get(req.componentPath, function success (err, result) {

		if (err) {
			res.sendStatus(err.headers.status)
			
		} else {
			res.json(result)
		}
	})
}