module.exports = function (req, res, next) {
	req.componentPath = ['component', req.params.component, req.params.id].join('/')
	next()
}