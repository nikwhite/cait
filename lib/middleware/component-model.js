var _ = require('lodash')
var logger = new (require('../util/logger'))('ComponentModel');
var registry = require('../modules/registry')
var config = require('../../config/config')
var path = process.cwd() + config.componentDir
/**
 * Find all the user-created page templates in directory 
 * provided by the user in config file (for now static dir)
 * @return {[type]} [description]
 */
function getComponentModel(req, res, next) {
	
	logger.log('Request for model: ' + req.params.component)

	res.json( registry.getComponent(req.params.component).model );
	
}

module.exports = getComponentModel