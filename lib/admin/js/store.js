/**
 * Store creates or extends an object (with WebComponents, this is typically a node), 
 * @class
 * @param {Object} config - configuration options
 * @param {Object} config.actions - A POJO with actionTypes as key names and functions as handlers
 * @param {Object} config.store - Where updated data will be stored
 * @param {Object} config.properties - A POJO with property names as data keys to bind to, and 
 *                                     values as defaults for the prop. These get updated automatically
 *                                     in the store if the payload contains a matching key
 * @returns {Store}
 */
function Store (config) {

    function dispatcherCallback(payload) {
        var actionType = payload.actionType;
        
        if ( this.actions[actionType] ) {
        
            Object.keys(this.properties).forEach(function (prop){
                if ( typeof payload[prop] !== 'undefined' )  {
                    this.store[prop] = payload[prop]
                }
            }.bind(this))

            if ( typeof this.actions[actionType] === 'function' ) {
                this.actions[actionType].call(this.store, payload);
            }   
        
        }
        
    }
    /** @member {Object} - stored config.store */
    this.store = config.store
    /** @member {Object} - stored config.actions */
    this.actions = config.actions
    /** @member {Object} - stored config.properties */
    this.properties = config.properties
    /** @member {Number} - the dispatch token for the Store */
    this.dispatchToken = Cait.dispatcher.register( dispatcherCallback.bind(this) )

}

