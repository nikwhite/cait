
;(function(){


	var Router = require('ampersand-router')
	var Dispatcher = require('flux').Dispatcher
	var Validator = require('jsonschema').Validator
	var _ = require('lodash')
	var mix = require('object-mix')
	var app = window.app

	////////////////
	/// ACTIONS
	////////////////
	
	Cait.viewActions = {
		goHome: function () {
			Cait.dispatcher.dispatch({
				actionType: 'goHome'
			})
		},
		createComponent: function (componentName) {
			Cait.dispatcher.dispatch({
				actionType: 'createComponent',
				componentName: componentName || null
			})
		},
		editComponent: function (componentName, componentId) {
			Cait.dispatcher.dispatch({
				actionType: 'editComponent',
				componentName: componentName,
				componentId: componentId
			})
		}
	}

	////////////////
	/// ROUTING
	////////////////	

	Cait.Router = Router.extend({
		routes: {
			'': 'home',
			'create-component': 'createComponent',
			'create-component/:componentName': 'createComponent',
			'edit-component/:componentName/:componentId': 'editComponent'
		},

		home: Cait.viewActions.goHome,

		createComponent: Cait.viewActions.createComponent,

		editComponent: Cait.viewActions.editComponent
	})

	Cait.router = new Cait.Router()
	Cait.dispatcher = new Dispatcher()

	////////////////
	/// STORE
	////////////////

	Cait.appStore = new Store({
		store: app,
		properties: {
			page: 'home',
			toolbarTitle: 'admin'
		},
		actions: {
			createComponent: function (payload) {
				this.page = 'create-component'
				if (payload.componentName) {
					this.toolbarTitle = 'new ' + payload.componentName	
				} else {
					this.toolbarTitle = 'choose a component'
				}
			},
			editComponent: function (payload) {
				this.page = 'edit-component'
				if (payload.componentName && payload.componentId) {
					this.toolbarTitle = 'edit ' + payload.componentName
				}
			},
			goHome: function () {
				this.page = 'home'
				this.toolbarTitle = 'admin'
			}

		}
	})

	////////////////
	/// INIT
	////////////////
	
	Cait.validator = new Validator()

	window.addEventListener('WebComponentsReady', function(){
		Cait.router.history.start({
			pushState: false,
			root: '/admin/'
		})
	})
})();
