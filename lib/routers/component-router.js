
var express = require('express');
var _ = require('lodash');
var pageRouter = express.Router();
var dbInterface = require('../db/db');
var db = dbInterface.db;
var util = require('util');
var Component = require('../modules/component');
var logger = new (require('../util/logger'))('ComponentRouter');

function updateRoutes() {
	
	db.view('pages/getRoutes', function(err, docs){
		// reset the express internal stack
		// TODO: Reset each individually so requests dont get dropped
		pageRouter.stack = [ ];

		if (err || !docs || !docs.length) {
			logger.log('No documents found for route mappings');
			return;
		} 

		// docs.forEach( function (doc) {
		// 	// Bring in the page object defined in JS and merge with db store
		// 	var path = require.resolve('../pages/' + doc.name);
		// 	var page;

		// 	// deleted the cached object so we can retrieve it fresh from the file
		// 	require.cache[path] = null;

		// 	page = require(path);

		// 	if ( page.data.route && page.render ) {
		// 		logger.log('Route created for %s', page.data.route);
		// 		pageRouter.get(page.data.route, page.render.bind(page));
		// 	} else {
		// 		logger.log('Page %s did not create a Page instance correctly.', doc._id );
		// 		logger.log( '=========Doc==========');
		// 		console.log(doc);
		// 		logger.log( '=========Page=========');
		// 		console.log(page);
		// 		logger.log( '=========END==========');
		// 	}
		// });
	});
}

dbInterface.on('ready', function () {
	// interval to config
	//setInterval(updateRoutes, 10000);
	updateRoutes();
});

module.exports = pageRouter;
