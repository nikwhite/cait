var express = require('express')
var fs = require('fs')

var config = require('../../config/config')
var logger = new (require('../util/logger'))('AdminRouter')
var registry = require('../modules/registry')

var adminRouter = express.Router()
var adminApiRouter = express.Router()

adminRouter.use( function (req, res, next) {
	// authentication middleware
	next()
})

adminApiRouter.get( '/list-components',             require('../middleware/list-components') )
adminApiRouter.get( '/component-model/:component',  require('../middleware/component-model') )
adminApiRouter.use( '/component',                   require('body-parser').json() )

adminApiRouter.route('/component/:component/:id')
              .all(  require('../middleware/component-path') )
              .head( require('../middleware/component-exists') )
              .get(  require('../middleware/get-full-component') )
              .post( require('../middleware/create-component') )
              .put(  require('../middleware/edit-component') )

adminRouter.use('/api', adminApiRouter)
adminRouter.use('/',    express.static( __dirname + '/../admin' ) )

module.exports = adminRouter