var express = require('express');
var db = require('../db/db').db;
var transport = require('../util/transport');
var logger = new (require('../util/logger'))('API');

var apiRouter = module.exports = express.Router();

apiRouter.route('/component/:component/:id')
         .all( require('../middleware/component-path') )
         .get( require('../middleware/get-component') )

apiRouter.get('view/:category/:view', function (req, res) {
	var view = req.params.category + '/' + req.params.view;

	db.view(view, function (err, result) {
		if (err) {
			logger.log('Error getting view: ' + view, err);
		} else {
			res.json(result)
		}
	});
});

