var _ = require('lodash')
var db = require('../db/db').db
var config = require('../../config/config')
var logger = new (require('../util/logger'))('Registry')
var Component = require('./component')
var fsUtil = require('../util/fs')
var inspect = require('util').inspect

var path = process.cwd() + config.componentDir
var instance

/**
 * Manages a registry of user-defined components and bootstraps 
 * all user-defined component models
 * @singleton
 * @todo - watch files for changes and flush the require cache for that file
 */
function Registry() {
	this.components = { }
	this.instances = { }
}

/**
 * [loadComponents description]
 * @return {[type]} [description]
 */
Registry.prototype.loadComponents = function () {
	return fsUtil.directoryList(path).then( 

		function success (componentList) {

			componentList.forEach(this.registerComponent.bind(this))
		
		}.bind(this)
	)
},

/**
 * [registerComponent description]
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
Registry.prototype.registerComponent = function (name) {
	logger.log('Registering component: %s', name)

	return this.addComponent(this.componentFactory(name))
}

/**
 * [componentFactory description]
 * @param  {[type]} name [description]
 * @param  {[type]} id   [description]
 * @param  {[type]} data [description]
 * @param  {[type]} rev  [description]
 * @return {[type]}      [description]
 */
Registry.prototype.componentFactory = function (name, id, data, rev) {
	logger.log('Creating component: %s', name)
	var ModelDefinition = require(path + '/' + name + '/' + name)
	
	return component = new Component({
		_rev: rev,
		id: id,
		name: name,
		data: data,
		model: new ModelDefinition()
	})
}

/**
 * [addComponent description]
 * @param {[type]} component [description]
 */
Registry.prototype.addComponent = function (component) {
	logger.log('Adding component: %s', component.name)
	return this.components[component.name] = component
}

/**
 * [getComponent description]
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
Registry.prototype.getComponent = function (name) {
	if (this.components[name]) {
		return this.components[name]
	} else {
		return this.registerComponent(name)
	}
}

/**
 * [createAndSave description]
 * @param  {[type]} name [description]
 * @param  {[type]} id   [description]
 * @param  {[type]} data [description]
 * @param  {[type]} rev  [description]
 * @return {[type]}      [description]
 */
Registry.prototype.createAndSave = function (name, id, data, _rev) {
	
	var component = this.componentFactory(name, id, data, _rev)
	var result = component.validate()

	return new Promise( function (resolve, reject) {

		logger.log('Creating %s with data: \n' + inspect(data), component._id)

		if (result.errors.length) {
			logger.log('Issues with component data: \n' + inspect(result.errors))
			return reject(400)
		}

		db.save(component.save(), function (err, result) {
			if (err || !result.ok) {
				logger.log('Error saving %s: ' + inspect(err), component._id)
				reject(err.headers.status)

			} else if (result.ok) {
				logger.log('Created: %s', component._id)
				resolve()
			}
		})
	})
}

module.exports = instance = new Registry();