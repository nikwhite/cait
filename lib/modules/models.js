var _ = require('lodash');
var fs = require('fs');

var textDefaults = {
	type: 'string',
    minLength: 2,
    maxLength: 256 
};

/**
 * [Model description]
 * @param {Sring} type           Used to lookup element html file for editing the data
 * @param {Object} defaultSchema Implements JSON Schema: http://tools.ietf.org/html/draft-zyp-json-schema-03
 * @param {Object} schema        Implements JSON Schema: http://tools.ietf.org/html/draft-zyp-json-schema-03
 */
function modelFactory(type, defaultSchema, schema) {
	schema = schema || {}
    return {
        type: type,
        schema: _.defaults(schema, defaultSchema)
    }
}

module.exports = {
    text: _.partial(modelFactory, 'text', textDefaults)
}