var _ = require('lodash')
var util = require('util')
var mix = require('object-mix')
var validator = require('./validator')
var logger = new (require('../util/logger'))('Component')
var deepDefaults = require('../util/deepDefaults')
var deepExtend = require('../util/deepExtend')
/**
 * [Component description]
 * @param {Object} config - configuration object for the component
 * @param {String} config.name - Name of the component
 * @param {String} config.id - Unique ID for the component instance
 * @param {Object} config.model - Result of new [ComponentName] from the User's component directory 
 * @param {Object} [config.data] - Any initial data to attach to the component                     
 */
function Component(config) {
    this.name = config.name 
    
    this.model = config.model
    
    this.schema = this.registerSchema( config.name, config.model )

    if (config.id) {
        this.id = config.id
        this._id = config._id || ['component', this.name, this.id].join('/')
    }
    
    if (config.data) {
        this.data = config.data
    }

    if (config._rev) {
        this._rev = config._rev
    }
}

/**
 * dbFields - only these component instance properties get saved in the database
 * @type {Array}
 */
Component.prototype.dbFields = [
    '_id',
    '_rev',
    'id',
    'data',
    'name',
]

/**
 * registerSchema takes the output from Models assigned to keynames and 
 * genrates a JSON Schema for validating the component. memoized
 * @param  {[type]} schema [description]
 * @return {[type]}        [description]
 */
Component.prototype.registerSchema = function (name, model) {
    var schema = {
        id: '/component/' + name,
        type: 'object',
        properties: _.mapValues(model, function (field) {
            return field.schema
        }),
        additionalProperties: false
    }

    validator.addSchema(schema, schema.id)

    return schema
}

/**
 * [validateModel description]
 * 
 * @return {[type]} [description]
 * @todo XSS prevention
 */
Component.prototype.validate = function (data) {
    return validator.validate(this.data, this.schema)
}

/**
 * Pulls object properties defined in the schema from the Component instance
 * @return {Object} the Component with only the properties defined in dbFields
 */
Component.prototype.save = function () {
    return _.partial(_.pick, this).apply( null, this.dbFields )
}

module.exports = Component;