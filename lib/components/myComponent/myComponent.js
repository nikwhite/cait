var models = require('../../modules/models')

module.exports = function MyComponent() {
	this.headline = models.text({
		maxLength: 40,
		required: true
	})
	this.introText = models.text()
}