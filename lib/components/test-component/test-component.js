var models = require('../../modules/models')

module.exports = function MyComponent() {
	this.someText = models.text({
		maxLength: 40,
		required: true,
		pattern: '^[a-zA-Z]*$'
	})
}