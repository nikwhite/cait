var models = require('../../modules/models')

module.exports = function MyComponent() {
	this.someNumber = models.text({
		maxLength: 40,
		minLength: 0,
		required: true,
		pattern: '^[0-9]*$'
	})
}