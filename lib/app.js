
var express = require('express');
var adminRouter = require('./routers/admin-router');
var componentRouter = require('./routers/component-router');
var config = require('../config/config');
var initApp = require('./init');
var apiRouter = require('./routers/api-router');
var logger = require('morgan');
var server;

initApp();

var app = express();

app.use(logger('common'));
app.use('/admin', adminRouter);
app.use('/api', apiRouter);
app.use('/', componentRouter);

server = app.listen(config.app_port, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('App listening at http://localhost:%s', port);

});

module.exports = {
	Component: require('./modules/component'),
	models: require('./modules/models')
};