
module.exports = {
	json: function (res, statusCode, data) {
		res.writeHead(statusCode, {'Content-type': 'application/json'});
		res.write(data);
		res.end();
	},
	html: function(res, data) {
		res.writeHead(200, {'Content-type': 'text/html'});
		res.write(data);
		res.end();
	},
	error: function (res) {
		return res.status(500).end()
	}
};