var fs = require('fs')
var _ = require('lodash')

var fsUtil = module.exports = {
	rejectDotFiles: function (name) {
		return name.charAt(0) !== '.';
	},
	directoryList: function (dir) {
		return new Promise( function (resolve, reject) {
			fs.readdir(dir, function (err, list) {
				if (err) reject(err)
				resolve( _.filter(list, fsUtil.rejectDotFiles) )
			})
		})
	},
	trimExtension: function (name) {
		var extensionIndex = name.lastIndexOf('.');
		return extensionIndex > 0 ? 
			name.substring(0,  extensionIndex) : name
	}
}