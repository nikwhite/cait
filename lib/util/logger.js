var _ = require('lodash');

function Logger(module) {
	this._module = module;
}

Logger.prototype.log = function (msg) {
	var log = _.partial(console.log, '[' + this._module + ']: ' + msg);
	var args = Array.prototype.slice.call(arguments, 1);

	if ( args.length >= 1 ) {
		log.apply(console, args );
	} else {
		log();
	}
};

module.exports = Logger;