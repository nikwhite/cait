
var _ = require('lodash');

function deepDefaults(obj) {
	if (!_.isObject(obj)) return obj;
    for (var i = 1, length = arguments.length; i < length; i++) {
		var source = arguments[i];
		for (var prop in source) {
			if (obj[prop] === void 0) {
				obj[prop] = source[prop];
				
			} else if ( _.isObject(source[prop]) && _.isObject(obj[prop]) ) {
				obj[prop] = deepDefaults(obj[prop], source[prop]);
			}
		}
    }
    return obj;
}

module.exports = deepDefaults;