var http = require('http')
var fs = require('fs')
var util = require('util')
var _ = require('lodash')
var q = require('q')
var cradle = require('cradle')
var EventEmitter = require('events').EventEmitter
var mix = require('object-mix')

var logger = new (require('../util/logger'))('DB')

var config = require('../../config/config')

var designDocDirFS = './lib/db/design'
var designDocDirRequire = '../db/design/'

var pageDirFS = './lib/pages'
var pageDirRequire = '../pages/'

var dbConnection
var db 

// extract to config
var cradleConfig = {
	host: config.db_ip,
	port: config.db_port,
	raw: false,
	forceSave: false,
	cache: false
}

cradle.setup( cradleConfig )
dbConnection = new cradle.Connection()
db = dbConnection.database(config.databaseName)

function createOrVerifyDB(database) {
	var deferred = q.defer()
	
	database.exists(function(err, exists){
		if (err) {
			logger.log( 'Error verifying database ' + database )
			logger.log(err)
			deferred.reject(err)

		} else if (exists) {
			logger.log( 'Database %s exists', database )
			deferred.resolve()

		} else {
			database.create()
			logger.log( 'Database %s created', database )
			deferred.resolve()
		}
	})

	return deferred.promise
}

/**
 * dbInterface 
 * @type {Object}
 * @mixes EventEmitter
 * Emits 'ready' when the verify() member function completes
 */
var dbInterface = module.exports = {

	/**
	 * [dbs description]
	 * @type {cradle.Connection.database}
	 */
	db: db,
	
	/**
	 * verify checks for the existence of all databases in dbs
	 * Emits a 'ready' event on dbInterface
	 */
	verify: function () {
		logger.log('Verifying databases...')
		
		createOrVerifyDB(db)
		    .then( function(){
		    	return q.all([
			    	this.importDocsFromDir( designDocDirFS, designDocDirRequire)
		    	])
	    	}.bind(this))
		 	.then( this.emit.bind(this, 'ready') )
	},

	/**
	 * docExists checks for the existence of a document in a given database
	 * @param  {String} docPath - Path to the document
	 * @return {Promise} Rejects if the document doesn't exist, Resolves
	 *     with the document revision if it does exist
	 */
	docExists: function (docPath) {
		var database
		var deferred = q.defer()

		if (!docPath) {
			deferred.reject()
			return
		}

		this.db.query({
			method: 'HEAD', 
			path: '/' + encodeURIComponent(docPath), 
		}, function (err, res, status) {
			if (err) {
				logger.log('Error: ' + err)
				deferred.reject()
				return
			}
			if (status === 200) {
				//strip double quotes from etag
				deferred.resolve( res.etag.replace(/"/g, '') )
			} else {
				deferred.reject()
			}
		})

		return deferred.promise
	},

	/**
	 * [importDocsFromDir description]
	 * @param  {[type]} fsDir       [description]
	 * @param  {[type]} requirePath [description]
	 * @return {[type]}             [description]
	 */
	importDocsFromDir: function (fsDir, requirePath) {
		logger.log('Importing documents from directory: %s', fsDir)
		var self = this
		var deferred = q.defer()

		function processFiles(files) {
			var promises = [ ]

			files.forEach(function (file) {
				promises.push( self.importDocFromFile(requirePath + file) )
			})

			return q.all(promises)
		}

		return q.nfcall(fs.readdir, fsDir)
			.then( processFiles )
	},

	/**
	 * [importDocFromFile description]
	 * @param  {String} path    path to the document
	 * @return {Promise}        promised value is the revision {String} 
	 */
	importDocFromFile: function (path) {
		var deferred = q.defer()
		var doc = require(path)

		logger.log('Updating doc: ' + doc._id )

		this.docExists(doc._id)
		 .then( function (rev) {
			doc._rev = rev
			logger.log('Doc %s exists, updating from revision %s...', doc._id, rev)

		 }, function(){
			logger.log('Doc %s doesn\'t exist, creating new...', doc._id)

		 })
		 .then( function () {

			this.db.save(doc, function(err, res){
				if ( err ) {
					logger.log('Doc %s not saved', doc._id, err)
					deferred.reject(doc._id)
				} else if ( res.ok ) {
					logger.log('Doc %s saved. Revision: %s', doc._id, res.rev)
					deferred.resolve(res.rev)
				}
			})

		 }.bind(this) )

		return deferred.promise
	}
}

mix( EventEmitter ).into( module.exports )

dbInterface.on('ready', function(){
	logger.log('Database ready')
});