module.exports = {
	_id: '_design/pages',
	views: {
		'listPages': {
			map: function (doc) {
				if ( doc.data.title ) {
					emit(doc.data.title, doc);
				}
			}
		},
		'getRoutes': {
			map: function(doc) {
				if ( doc.data.route ) {
					emit(doc._id, doc);
				}
			}
		}
	}
};