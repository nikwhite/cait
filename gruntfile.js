module.exports = function(grunt) {
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        browserify: {
            admin: {
                src: 'lib/admin/js/app.js',
                dest: 'lib/admin/js/app.bundled.js'
            }
        },

        watch : {
            js: {
                files: ['lib/admin/js/**'],
                tasks: ['default'],
                options: {
                    debounceDelay: 1000
                }
            }
        }
    })

    grunt.loadNpmTasks('grunt-browserify')
    grunt.loadNpmTasks('grunt-contrib-watch')

    grunt.registerTask('default', ['browserify'])
}