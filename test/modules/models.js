var _ = require('lodash');
var models = require('../../lib/modules/models');

describe('Models', function(){
	it('should be an object made up of functions', function(){
		models.should.be.an.Object;	
		_.forEach(models, function (model) {
			model.should.be.a.Function;
		})
	})

	describe('#addModel', function () {
		
	})

	describe('#text()', function(){
		var instance = models.text();
		it('returns an object with prop "type" as "text"', function(){
			instance.type.should.equal('text')	
		})
		it('returns an object with prop "schema" matching the API for schema-validator', function(){
			var schema = instance.schema;
			var typeResult;

			schema.type.should.be.exactly('string')

			if (schema.length) {
				schema.minLength && schema.minLength.should.be.a.Number
				schema.maxLength && schema.maxLength.should.be.a.Number
			}
		})
	})
})