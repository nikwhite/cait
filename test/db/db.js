var cradle = require('cradle');
var _ = require('lodash');

var dbInterface = require('../../lib/db/db');
var testDocId = 'testFileDoc';
var testDoc = {
	testData: 'SomeData'
};
var testDb, c;

cradle.setup({
	cache: false,
	raw: false,
	forceSave: false
});
c = new(cradle.Connection);

function createTestDb(done) {
	return new Promise(function (resolve, reject) {
		var db = c.database('testdb')

		db.exists(function (err, exists){
			if (err) {
				resolve(err)
			} else if (exists) {
				db.destroy()
			}
			db.create(function(){
				resolve(db)
			})
		})
	})
}

function parseRev(revStr){
	return parseInt(revStr.charAt(0), 10)
}

describe('dbInterface', function (){

	before(function (done){
		createTestDb()
		 .then(function (db){ testDb = db })
		 .then(done, done)
	})

	it('should be an object', function (){
		dbInterface.should.be.an.instanceOf(Object).and.have.property('db')
	})

	describe('db', function (){
		it('should be up', function (){
			dbInterface.db.should.be.ok
			dbInterface.db = testDb
			dbInterface.db.should.be.ok
		})
	})

	describe('#docExists()', function (){

		it('should fail if doc doesnt exist', function (done){
			dbInterface.docExists('test/doc').then(function (rev){
				done(new Error('test/doc should not exist'))
			}).fail( function (){
				done()
			})
		})

		it('should return the etag if a document exists in the db', function (done){
			dbInterface.db.save('test/doc', testDoc, function (){
				dbInterface.docExists('test/doc').then(function(rev){
					rev.should.be.a.String
					done()
				}, function(){
					done(new Error('test/doc should return an etag'))
				})
			})
		})
	})
  	
  
	describe('#importDocFromFile()', function () {
		var rev1;
		var testFileDoc = require('./testFileDoc');

		it('should create a database document from a file', function (done){
			dbInterface.importDocFromFile(__dirname+'/testFileDoc').then(function (rev) {
				rev1 = parseRev(rev);
				rev.should.be.a.String;
				done()
			}, function () {
				done(new Error('doc not created'))
			})
		})

		it('new database doc from file should match the file', function (done){
			dbInterface.db.get(testFileDoc._id, function (err, doc){
				if ( err ){
					done(new Error('database doc doesn\'t match file ::DB_ERR:: ' + err ))
				} else {
					doc.should.have.properties(testFileDoc);
					done();
				}
			})
		})

		it('should update an existing document from the file', function (done){
			dbInterface.importDocFromFile(__dirname+'/testFileDoc').then(function (rev) {
				parseRev(rev).should.equal(rev1+1);
				done()
			}, function () {
				done(new Error('doc not updated'))
			})
		})
	})

	after(function(){
		testDb.destroy()
	})
})